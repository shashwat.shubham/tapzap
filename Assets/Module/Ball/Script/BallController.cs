using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallController : MonoBehaviour
{
    public GameObject BlinkTextObject;
    public Transform PlatformDetectorTransform;
    public LayerMask Layer;
    public GameObject ScoreTextHolder;
    [SerializeField] private float speed;
    
    bool started;
    bool gameOver;
    Rigidbody rb;
    bool moveX = true;

    // Start is called before the first frame update
    void Awake()
    {
        rb = GetComponent<Rigidbody>();
        Application.targetFrameRate = 120;
      
    }

    private void Start()
    {
        started = false; // game has not started yet / screen not touched / mouse not clicked 
        gameOver = false;
    }

    public void OnGameStarted()
    {
        this.GetComponent<Rigidbody>().isKinematic = false;
        GameManager.instance.GameStart();
        started = true;
        rb.angularVelocity = Vector3.zero;
        SwitchDirection();
        //Add Score
        GameManager.instance.AddScore();
        BlinkTextObject.SetActive(false);
        ScoreTextHolder.SetActive(true);
    }

    // Update is called once per frame
    void Update()
    {
        if (!GameManager.instance.GetIsGameRunning()) return;

        if (Input.GetMouseButtonDown(0))
            SwitchDirection();

        if (rb.velocity.magnitude != 0)
        {
            if (moveX == false && rb.velocity.x < speed)
                rb.velocity = new Vector3(speed, 0, 0);
            else if (moveX == true && rb.velocity.z < speed)
                rb.velocity = new Vector3(0, 0, speed);
        }
    }   
    void SwitchDirection()
    {
                
             if (moveX)
             {
            
            rb.velocity = new Vector3(speed, 0, 0) * Time.deltaTime;
            moveX = false;
             }
             else if (!moveX)
             {
            
            rb.velocity = new Vector3(0, 0, speed) * Time.deltaTime;
            moveX = true;
             }
           else
           {
            //Ball is at rest, so move it
            rb.velocity = new Vector3(speed, 0, 0) * Time.deltaTime;//ball will go in x direction
            }
    }   

    public bool CheckX() 
    {
        return moveX;    
    }

}



