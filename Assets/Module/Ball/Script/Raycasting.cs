using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Raycasting : MonoBehaviour
{
    public Transform ball;
    public Vector3 RaycastOffset;

    public BallController BC;

    public float elapsedTime = 0;
    public float thresholdTime = 0; //This is the time till we want the player to play on false raycast
    [SerializeField]
    private float fallSpeed; // speed ball falls off platform at

    private void Start()
    {
        BC = ball.gameObject.GetComponent<BallController>();
    }

    private void Update()
    {
        if (BC.CheckX()) 
        {
            RaycastOffset.x = 0.5f;
        }
        else
        {
            RaycastOffset.x = -0.5f;
        }

        transform.position = ball.position;

        //Debug.DrawLine(ball.position + RaycastOffset, new Vector3(ball.position.x, ball.position.y - 10.0f, ball.position.z) + RaycastOffset, Color.red,2.5f);
    }
    private void LateUpdate()
    {
        if (!GameManager.instance.GetIsGameRunning()) return;

        Vector3 fwd = transform.TransformDirection(Vector3.down);


        
        if (!Physics.Raycast(ball.position, fwd, 10))
        {
            elapsedTime += Time.deltaTime;

            if (elapsedTime > thresholdTime)
            {
                GameManager.instance.GameOver();
                ball.gameObject.GetComponent<Rigidbody>().velocity = new Vector3(0, -fallSpeed, 0);
                AudioManager.Instance.PlayGameOverMusic();
                AudioManager.Instance.StopBGMusic();
               // Destroy(ball.gameObject, 0.3f);
            }
        }
        else
        {
            elapsedTime = 0;
        }
    }
}
