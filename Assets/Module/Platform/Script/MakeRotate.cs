using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MakeRotate : MonoBehaviour
{
    [SerializeField] private float speed;

    private void Update()
    {
        if (speed > 0)
        transform.Rotate(Vector3.up * speed * Time.deltaTime);
    }
}
