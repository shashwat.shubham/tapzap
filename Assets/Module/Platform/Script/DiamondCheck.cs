using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DiamondCheck : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Ball")
        {
            //Collec special
            GameManager.instance.CollectSpecial();

            //Hide the mesh renderer
            this.GetComponent<MeshRenderer>().forceRenderingOff = true;

            //Activate the particle system in the child
            //this.GetComponentInChildren<ParticleSystem>().Play();

            //Play sound
            AudioManager.Instance.PlayDiamondCollectedMusic();

            Destroy(gameObject, 0.4f);
        }
    }
}
