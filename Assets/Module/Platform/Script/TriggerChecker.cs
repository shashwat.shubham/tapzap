using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerChecker : MonoBehaviour
{
    // Start is called before the first frame update
    void OnTriggerExit(Collider col)
    {
        if (col.gameObject.tag == "Ball")

            Invoke("FallDown", 0.2f);
    }

    void FallDown()
    {
        GameObject parentObject = transform.parent.gameObject;
        parentObject.AddComponent<Rigidbody>();

        //Function to check and generate more paths
        FindObjectOfType<PlatformSpawner>().CheckAndAddPath();

        Destroy(transform.parent.gameObject, 2f);
    }
}