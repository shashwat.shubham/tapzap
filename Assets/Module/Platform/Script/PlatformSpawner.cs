using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class PlatformSpawner : MonoBehaviour
{
    public GameObject platform;
    //public GameObject thinnerPlatformX;
    //public GameObject thinnerPlatformZ;
    public GameObject thinPlatform;
    public GameObject Diamond;

    [SerializeField] Vector3 lastPosition;//last position of plateform
    float size;//size of platform 
    int currentPathCount = 0;
    private GameObject lastPathBlock;

    // Start is called before the first frame update
    void Start()
    {
        //lastPosition = platform.transform.position;
        size = platform.transform.localScale.x; // x or z, both are the same	

        for (int i = 0; i < 40; i++) // create first set of platforms
            SpawnPlatform();

    }

    internal void CheckAndAddPath()
    {
        if(currentPathCount < 39)
        {
            //Generate more platforms
            for (int i = 0; i < 40; i++) // create first set of platforms
                SpawnPlatform();
        }
        else
        {
            currentPathCount -= 1;
        }
    }

    private void SpawnPlatform()
    {
        int rand = Random.Range(0, 6);// 0 to 5
        if (rand < 3)
            SpawnX();
        else if (rand >= 3)
            SpawnZ();

        currentPathCount += 1;

        _spawnDiamond();
    }

    private void SpawnZ()
    {
        Vector3 pos = lastPosition;
        
        GameObject itemToSpawn = (GameManager.instance.GetPlayerScore() > 5) ? thinPlatform : platform;
        pos.z += itemToSpawn.transform.localScale.z;
        lastPathBlock = Instantiate(itemToSpawn, pos, Quaternion.identity);
        lastPosition = pos;



    }

    private void SpawnX()
    {
        Vector3 pos = lastPosition;
        
        GameObject itemToSpawn = (GameManager.instance.GetPlayerScore() > 5) ? thinPlatform : platform;
        pos.x += itemToSpawn.transform.localScale.x; // move new one on x axis by the size of the platform
        lastPathBlock = Instantiate(itemToSpawn, pos, Quaternion.identity);
        lastPosition = pos;
    }


    private void _spawnDiamond()
    {
        int result = Random.Range(1, 20);

        if(result < 5)
        {
            //Spawn diamond
            Vector3 spawnPosition = lastPathBlock.transform.GetChild(1).position;
            Instantiate(Diamond, spawnPosition, Quaternion.identity);
        }
    }

}
