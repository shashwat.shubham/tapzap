using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class UIManager : MonoBehaviour
{
    public Text ScoreText;
    public Text HighScoreText;
    
    public GameObject GameOverPanel;
    // public Button RestartBtn;

    
    public void UpdateScoreText(string score)
    {
        //ScoreText.text = "Score : " + score;
        ScoreText.text = score;
        
    }

    public void UpdateHighScoreText(string highScore)
    {
        HighScoreText.text=highScore;
    }

    
    public void ShowGameOverPanel()
    {
        Time.timeScale = 0;
        GameOverPanel.SetActive(true);
    }

    public void RestartBtn() 
    {

        //Show the ad
        Interstitial.instance.ShowInterstitialAd();

        SceneManager.LoadScene(0);
        Time.timeScale = 1;
    }
}
