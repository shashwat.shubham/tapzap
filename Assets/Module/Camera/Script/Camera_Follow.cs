using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Camera_Follow : MonoBehaviour
{
    // Start is called before the first frame update
    public GameObject ball;
    Vector3 offset; // make camera stay certain distance from ball when following
    public float lerpRate; // rate camera will change position to follow ball

    private void Start()
    {
        offset = ball.transform.position - transform.position;
    }

    private void Update()
    {
        if (!GameManager.instance.GetIsGameRunning()) return;
        Follow();
    }

    private void Follow()
    {
        Vector3 pos = transform.position;//Current camera Position
        Vector3 targetPos = ball.transform.position - offset;//Distance from ball
        pos = Vector3.Lerp(pos, targetPos, lerpRate * Time.deltaTime);//lerp moves on from one value to another value at a certain rate
        transform.position = pos;
    }
}
