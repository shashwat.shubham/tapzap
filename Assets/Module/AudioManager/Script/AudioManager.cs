using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AudioManager : MonoBehaviour
{
    public AudioSource BgMusic;
    public AudioSource DiamondCollectedMusic;
    public AudioSource GameOverMusic;

    public Sprite AudioOnImage;
    public Sprite AudioOffImage;
    public Image SoundButtonImage;

    private bool canPlaySound;

    public static AudioManager Instance;
    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }

        _initializeSoundSetup();
    }

    private void _initializeSoundSetup()
    {
        canPlaySound = PlayerPrefs.GetInt("USER_CAN_PLAY_AUDIO", 1) == 1 ? true : false;

        SoundButtonImage.sprite = canPlaySound ? AudioOnImage : AudioOffImage;
    }

    public void ToggleAudio()
    {
        canPlaySound = !canPlaySound;
        int audioVal = canPlaySound ? 1 : 0;

        SoundButtonImage.sprite = canPlaySound ? AudioOnImage : AudioOffImage;

        PlayerPrefs.SetInt("USER_CAN_PLAY_AUDIO", audioVal);

        if (audioVal == 1)
            PlayBGMusic();
        else
            StopBGMusic();
    }

    //Player Run Sound Play
    public void PlayBGMusic()
    {
        if (!BgMusic.isPlaying && canPlaySound)
            BgMusic.Play();
    }
    //Diamond PlaySound
    public void PlayDiamondCollectedMusic()
    {
        if(canPlaySound)
        DiamondCollectedMusic.Play();
    }
    //GameOver Sound 
    public void PlayGameOverMusic()
    {
        if(canPlaySound)
        GameOverMusic.Play();
    }

    //Stop Bg Music
    public void StopBGMusic()
    {
        if (BgMusic.isPlaying)
            BgMusic.Stop();
    }
}
