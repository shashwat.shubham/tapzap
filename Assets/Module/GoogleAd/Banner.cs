using GoogleMobileAds;
using GoogleMobileAds.Api;
using System;
using Unity.VisualScripting;
using UnityEngine;

public class Banner : MonoBehaviour
{
    BannerView _bannerView;
    public void Start()
    {
        // Initialize the Google Mobile Ads SDK.
        MobileAds.Initialize((InitializationStatus initStatus) =>
        {
            // This callback is called once the MobileAds SDK is initialized.
        });

        CreateBannerView();
    }
#if UNITY_ANDROID
    private string _adUnitId = "ca-app-pub-3940256099942544/6300978111";
#elif UNITY_IPHONE
  private string _adUnitId = "ca-app-pub-3940256099942544/2934735716";
#else
  private string _adUnitId = "unused";
#endif
    
       

        /// <summary>
        /// Creates a 320x50 banner view at top of the screen.
        /// </summary>
         public  void CreateBannerView()
         {
            Debug.Log("Creating banner view");

            // If we already have a banner, destroy the old one.
            if (_bannerView != null)
            {
                // DestroyAd();
            }



        // Create a 320x50 banner at top of the screen
        //_bannerView = new BannerView(_adUnitId, AdSize.Banner, AdPosition.Center);
        _bannerView = new BannerView(_adUnitId, AdSize.Banner, 0,200 );

        //create an empty ad request
        AdRequest adRequest = new AdRequest();

            //Send Request to load add
            _bannerView.LoadAd(adRequest);
         }

    /// <summary>
    /// Destroys the banner view.
    /// </summary>
    public void DestroyBannerView()
    {
        if (_bannerView != null)
        {
            Debug.Log("Destroying banner view.");
            _bannerView.Destroy();
            _bannerView = null;
        }
    }
}
     




    
      






