
using UnityEngine;
using UnityEngine.UI;


public class GameManager : MonoBehaviour
{
    public static GameManager instance;
    public bool gameOver;
    private int playerScore=0;
    private int HighScoreText;
    private bool gameStarted = false;

    void Awake()
    {
        if (instance == null)
            instance = this;
    }


    void Start()
    {
        Application.targetFrameRate = 120;
        gameOver = false;
        //gameOver = true;
        //HighScore updated
        UpdatedHighScore();


        //Play audio if applicable
        AudioManager.Instance.PlayBGMusic();

        //Start loading the ad
        Interstitial.instance.InitializeAdLoad();
    }

    internal void CollectSpecial()
    {
        playerScore += 5;
        FindObjectOfType<UIManager>().UpdateScoreText(playerScore.ToString()) ;
        if(playerScore > PlayerPrefs.GetInt("Highscore",0))
        {
            PlayerPrefs.SetInt("Highscore", playerScore);
            FindObjectOfType<UIManager>().HighScoreText.text = playerScore.ToString();
        }
       
        
    }

    // Use this for initialization
   


    public void GameStart()
    {
        gameStarted = true;
    }

    public void GameOver()
    {
        gameOver = true;

        //Activate the panel
        Invoke("showGameOverPanel", 0.5f);

        


        gameStarted = false;
    }

    void showGameOverPanel() { FindObjectOfType<UIManager>().ShowGameOverPanel(); }

    public void AddScore()
    {
        //playerScore += 1;
        FindObjectOfType<UIManager>().UpdateScoreText(playerScore.ToString());
    }

    public void UpdatedHighScore()
    {
        FindObjectOfType<UIManager>().UpdateHighScoreText(PlayerPrefs.GetInt("Highscore", 0).ToString());
    }

    public bool GetIsGameRunning()
    {
        return gameStarted;
    }

    public int GetPlayerScore()
    {
        return playerScore;
    }

    public void QuitGame() 
    {
        Application.Quit();
    
    }
}
